<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 8]>
      <?php print phptemplate_get_ie7_styles(); ?>
    <![endif]-->
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>
  <body class="<?php print $body_classes; ?>">

<!-- Layout -->
  

    <div id="wrapper">
    <div id="container" class="clear-block">

      <div id="header">
        <div id="header-decorator-left">
        <div id="header-decorator-right">
        <div id="logo-floater">
          <h1> <a href="<?php print check_url($front_page); ?>">
            <?php if ($logo): ?> 
              <img src="<?php print check_url($logo); ?>" 
                   title="<?php print check_plain($site_name) . ' | ' . check_plain($site_slogan); ?>" 
                   alt="<?php print check_plain($site_name) . ' | ' . check_plain($site_slogan); ?>" id="logo" />
            <?php endif; ?>
            <span><?php print check_plain($site_name); ?></span>
          </a> </h1>
          <p class="slogan"> <?php print check_plain($site_slogan);?> </p>
        </div>
       
        <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
        <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?>
        </div>
        </div>
      </div> <!-- /header -->
      <div id="header-region" class="clear-block">

        <?php print $header; ?></div>
      <div id="d-top">  
      <div id="d-topleft">
      <div id="d-topright">

      <?php if ($left): ?>
        <div id="left-sidebar" class="sidebar">
          <?php print $left ?>
        </div>
      <?php endif; ?>

      <div id="center"><div id="squeeze">
          <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php print $help; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <div class="clear-block">
            <?php print $content ?>
          </div>
          <?php print $feed_icons ?>
      </div></div> <!-- /#squeeze, /#center -->

      <?php if ($right): ?>
        <div id="right-sidebar" class="sidebar">
          <?php print $right ?>
        </div>
      <?php endif; ?>

        <div id="footer">
          <div id="footer-decorator-topleft">
          <div id="footer-decorator-topright">
          <div id="footer-decorator-bottomleft">
          <div id="footer-decorator-bottomright">
          <?php print $footer_message . $footer ?>
          <div class="clear-block"></div>
          </div>
          </div>
          </div>
          </div>
        </div>
      </div>
      </div>
      </div> <!-- /decorators -->

      <div class="clear-block"></div>

    </div> <!-- /container -->

  </div>
<!-- /layout -->

  <?php print $closure ?>
  </body>
</html>
